Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Source: https://github.com/FedoraQt/adwaita-qt

Files: *
Copyright: 2014 Hugo Pereira Da Costa <hugo.pereira@free.fr>
License: GPL-2+

Files: src/demo/*
Copyright: 2014-2018 Martin Bříza <mbriza@redhat.com>
           2019 Jan Grulich <jgrulich@redhat.com>
License: GPL-2+

Files: src/lib/adwaita.cpp
       src/lib/adwaitacolors_p.h
       src/lib/adwaitadebug.h
       tests/test.*
Copyright: 2019-2021 Jan Grulich <jgrulich@redhat.com>
License: GPL-2+

Files: src/lib/adwaita.h
       src/lib/adwaitawidgetexplorer.cpp
Copyright: 2014 Hugo Pereira Da Costa <hugo.pereira@free.fr>
           2019 Jan Grulich <jgrulich@redhat.com>
License: GPL-2+

Files: src/lib/adwaitacolors.*
       src/lib/adwaitarenderer.*
       src/style/adwaitahelper.*
       src/style/adwaitastyle.*
Copyright: 2014 Hugo Pereira Da Costa <hugo.pereira@free.fr>
           2014-2018 Martin Bříza <m@rtinbriza.cz>
           2019-2021 Jan Grulich <jgrulich@redhat.com>
License: GPL-2+

Files: src/lib/adwaitawindowmanager.cpp
Copyright: 2014 Hugo Pereira Da Costa <hugo.pereira@free.fr>
           2007 Thomas Luebking <thomas.luebking@web.de>
License: GPL-2+ and Expat

Files: src/lib/animations/adwaitastackedwidgetdata.*
       src/lib/animations/adwaitastackedwidgetengine.*
       src/lib/animations/adwaitatransitiondata.*
       src/lib/animations/adwaitatransitionwidget.*
Copyright: 2009 Hugo Pereira Da Costa <hugo.pereira@free.fr>
License: Expat

Files: src/lib/animations/adwaitaheaderviewdata.cpp
       src/lib/animations/adwaitatabbardata.cpp
Copyright: 2009-2014 Hugo Pereira Da Costa <hugo.pereira@free.fr>
License: GPL-2+ and Expat

Files: .clang-format
Copyright: 2019 Christoph Cullmann <cullmann@kde.org>
           2019 Gernot Gebhard <gebhard@absint.com>
License: Expat

Files: cmake/FindXCB.cmake
Copyright: 2012 Fredrik Höglund <fredrik@kde.org>
License: BSD-3-clause

Files: debian/*
Copyright: 2016-2021 Dmitry Shachnev <mitya57@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the complete text of the GNU General Public
 License version 2 can be found in /usr/share/common-licenses/GPL-2.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to
 deal in the Software without restriction, including without limitation the
 rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 IN THE SOFTWARE.

License: BSD-3-clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 .
 1. Redistributions of source code must retain the copyright
    notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 3. The name of the author may not be used to endorse or promote products
    derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
